__author__ = 'ankush'

import csv
import MySQLdb


fo = open('datasets/crop-wise-all-india-position-requirement-and-availability.csv')
reader = csv.reader(fo)


db = MySQLdb.connect(host="localhost", user="root", passwd="", db="sequoia_hack")
cursor = db.cursor()

for row in reader:
	if row[0].lower()=='crop name':
		continue

	if 'total' in row[0].lower():
		continue
	if not row[0]:
		continue

	query = "insert into crop_requirement_and_availability (crop, requirement, availability, surplus) values ('%s', %s, %s, %s)" %(row[0],str(row[1]),str(row[2]),str(row[3]))
	print query
	cursor.execute(query)

db.commit()
