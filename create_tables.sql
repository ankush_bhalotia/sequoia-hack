create table met_data(data_type VARCHAR(40), state VARCHAR(100), district VARCHAR(100), year int, month CHAR(3), value double, PRIMARY KEY (data_type, state, district, year, month));
create table crop_requirement_and_availability(crop VARCHAR(40), requirement int, availability int, surplus int, PRIMARY KEY(crop));
create table minimum_support_price (crop VARCHAR(40), price int, PRIMARY KEY(crop));
create table crop_climate_requirement (crop VARCHAR(40), maxtemp int, mintemp int, rainfall int, PRIMARY KEY(crop));