__author__ = 'byom'

import pandas as pd
import json
import csv
from sklearn import tree
# from IPython.display import Image
# from sklearn.externals.six import StringIO



def getDataFrame(file_name):
	d_frame = pd.read_csv(file_name)
	return d_frame



def frameToDict(data_frame):
	attributes = list(data_frame.columns.values)
	d_dict = {}
	for attr in attributes:
		d_dict[attr] = list(data_frame[attr])
	return d_dict



def getFarmerData(person_data_column_order):
	person_input_data = []

	for each in person_data_column_order:
		print "KINDLY ENTER ",each," :....."
		inp = raw_input()
		person_input_data.append(inp)

	valid_data_flag = True
	while(valid_data_flag):
		if (len(person_input_data) == 9):
			valid_data_flag = False
		else:
			print "INVALID INPUT PLEASE ENTER IT AGAIN"
			person_input_data = raw_input()

	return person_input_data



def inputMatchingIndex(person_input_data,person_dict,person_data_column_order):
	attr_values = person_data_column_order
	attr_values_matching_index = []

	for idx in range(0,len(person_dict["age"])):
		match_cnt = 0
		for jdx,attr in enumerate(attr_values):
			# print person_input_data[jdx],"person data"
			# print person_dict[attr][idx],"history data"
			# raw_input()
			if (str(person_dict[attr][idx]) == str(person_input_data[jdx])):
				match_cnt += 1

			# print match_cnt

		if (match_cnt >=4):
			attr_values_matching_index.append(idx)

	return attr_values_matching_index



def getPersonCreditConfidence(matching_row,person_dict):
	default_status = person_dict["default_class"]
	completed_status_cnt = 0

	for idx in matching_row:
		if str(default_status[idx]) == "0":
			completed_status_cnt += 1
		else:
			pass
			# print default_status[idx]
			# raw_input()

	return float(completed_status_cnt)/float(len(matching_row))



def trainDecisionTree(person_dict,person_data_column_order):
	train_data_set = []
	class_label = []
	for idx in range(0,len(person_dict["default_class"])):
		cur_data = []
		for attr in person_data_column_order:
			cur_data.append(person_dict[attr][idx])
		train_data_set.append(cur_data)
		class_label.append(person_dict["default_class"][idx])


	return train_data_set,class_label




def drawImage(person_data_column_order,decision_model,class_label):
	dot_data = StringIO()
	tree.export_graphviz(decision_model,out_file = dot_data,feature_names = person_data_column_order,class_names = class_label
		,filled = True,rounded = True, special_characters = True)
	graph = pydot.graph_from_dot_data(dot_data.getvalue())
	Image (graph.create_png())





def get_confidence_testing():
	person_data_source = "farmer_database_set.csv"
	person_frame = getDataFrame(person_data_source)
	person_dict = frameToDict(person_frame)

	person_data_column_order = ["age","education_qualification","sex","farming_exp","previous_loan_availed","loan_status","income",
								"land_owned","criminal_history"]


	'''
	While integrating this with html:
	take "person_input_data" as an argument in main function and comment out "person_input_data = getFarmerData(person_data_column_order)"

	Make sure that person_input_data is in same order as person_data_column_order
	'''
	person_input_data = getFarmerData(person_data_column_order)
	matching_row = inputMatchingIndex(person_input_data,person_dict,person_data_column_order)
	person_confidence = getPersonCreditConfidence(matching_row,person_dict)

	print "We can grant loan to this person with :-", person_confidence,"confidence"


	# raw_input()

	# data_set,class_label = trainDecisionTree(person_dict,person_data_column_order)
	# decision_model = tree.DecisionTreeClassifier()
	# decision_model = decision_model.fit(data_set,class_label)

	# print decision_model.predict_proba([person_input_data])
	# # drawImage(person_data_column_order,decision_model,class_label)





def get_confidence(age, education_qualification, sex, farming_exp, previous_loan_availed, loan_status, income, land_owned, criminal_history):
	'''
		age --> age class (18-22 is class 1, 23-30 is class 2, 31-45 is class 3, 46-55 is class 4, 55+ is class 5)
		education_qualification --> educational qualification level (None is level 0, Primary is level 1, .., Undergraduate is level 4, Postgraduate is level 5)
		sex -->
		farming_exp -->

	'''
	person_data_source = "farmer_database_set.csv"
	person_frame = getDataFrame(person_data_source)
	person_dict = frameToDict(person_frame)

	person_data_column_order = ["age","education_qualification","sex","farming_exp","previous_loan_availed","loan_status","income",
								"land_owned","criminal_history"]


	person_input_data = [age, education_qualification, sex, farming_exp, previous_loan_availed, loan_status, income, land_owned, criminal_history]
	matching_row = inputMatchingIndex(person_input_data,person_dict,person_data_column_order)
	person_confidence = getPersonCreditConfidence(matching_row,person_dict)

	return person_confidence






if __name__ == '__main__':
	get_confidence_testing()



