-- MySQL dump 10.13  Distrib 5.7.11, for osx10.11 (x86_64)
--
-- Host: localhost    Database: sequoia_hack
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `crop_climate_requirement`
--

DROP TABLE IF EXISTS `crop_climate_requirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crop_climate_requirement` (
  `crop` varchar(40) NOT NULL DEFAULT '',
  `maxtemp` int(11) DEFAULT NULL,
  `mintemp` int(11) DEFAULT NULL,
  `rainfall` int(11) DEFAULT NULL,
  PRIMARY KEY (`crop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crop_climate_requirement`
--

LOCK TABLES `crop_climate_requirement` WRITE;
/*!40000 ALTER TABLE `crop_climate_requirement` DISABLE KEYS */;
INSERT INTO `crop_climate_requirement` VALUES ('ARHAR',35,30,62),('BAJRA',28,20,50),('BARLEY',17,15,45),('COTTON',28,25,90),('GRAM',25,18,75),('GROUNDNUT',33,30,100),('INDIAN BEAN',28,14,45),('JOWAR',32,27,55),('JUTE',37,21,140),('MAIZE',27,18,70),('MILLET',32,26,50),('MOONG',35,25,90),('MUSTARD',25,15,90),('PEAS',23,15,40),('POTATO',26,20,60),('RAGI',30,27,42),('RICE',37,25,150),('SOYABEAN',30,20,60),('SUGARCANE',30,27,60),('SUNFLOWER',25,21,40),('URAD',32,28,80),('WHEAT',20,18,65);
/*!40000 ALTER TABLE `crop_climate_requirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crop_requirement_and_availability`
--

DROP TABLE IF EXISTS `crop_requirement_and_availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crop_requirement_and_availability` (
  `crop` varchar(40) NOT NULL,
  `requirement` int(11) DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `surplus` int(11) DEFAULT NULL,
  PRIMARY KEY (`crop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crop_requirement_and_availability`
--

LOCK TABLES `crop_requirement_and_availability` WRITE;
/*!40000 ALTER TABLE `crop_requirement_and_availability` DISABLE KEYS */;
INSERT INTO `crop_requirement_and_availability` VALUES ('ARHAR',1390,6902,5512),('BAJRA',17450,36067,18617),('BARLEY',223293,286952,63659),('BERSEEM',2100,2100,0),('CASTOR',65,98,34),('COTTON',5028,5665,637),('COWPEA',3098,3691,593),('GOBHI SARSON',280,452,172),('GRAM',1611078,1572180,-38898),('GROUNDNUT',690972,828875,137903),('HORSE GRAM',9218,9821,603),('INDIAN BEAN',578,578,0),('ITALIAN MILLET',150,261,111),('JOWAR',106007,112793,6786),('KHESARI',5775,5775,0),('LENTIL',178890,138342,-40548),('LETHYRUS',595,542,-53),('LINSEED',11951,8046,-3905),('LITTLE MILLET',25,25,0),('MAIZE',287367,348659,61293),('MOONG',47510,73043,25533),('NIGER',1440,1440,0),('OAT',12000,13016,1016),('PADDY',2006662,2039415,32753),('PEAS',195664,156766,-38898),('POTATO',2719000,2484948,-234052),('R&M',242850,249169,6319),('RAGI',2377,3105,728),('RAJMASH',185,70,-115),('SAFFLOWER',11685,11937,252),('SESAME',7525,8114,589),('SOYABEAN',0,30,30),('SUNFLOWER',26284,26791,508),('TORIA',21237,20275,-962),('URAD',87153,89078,1925),('WHEAT',11252951,11685529,432578);
/*!40000 ALTER TABLE `crop_requirement_and_availability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `met_data`
--

DROP TABLE IF EXISTS `met_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `met_data` (
  `data_type` varchar(40) NOT NULL,
  `state` varchar(100) NOT NULL,
  `district` varchar(100) NOT NULL,
  `year` int(11) NOT NULL,
  `month` char(3) NOT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`data_type`,`state`,`district`,`year`,`month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `met_data`
--

LOCK TABLES `met_data` WRITE;
/*!40000 ALTER TABLE `met_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `met_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `minimum_support_price`
--

DROP TABLE IF EXISTS `minimum_support_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `minimum_support_price` (
  `crop` varchar(40) NOT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`crop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `minimum_support_price`
--

LOCK TABLES `minimum_support_price` WRITE;
/*!40000 ALTER TABLE `minimum_support_price` DISABLE KEYS */;
INSERT INTO `minimum_support_price` VALUES ('Arhar ',3850),('Bajra',1175),('Barley ',980),('Copra',5100),('Cotton',3600),('Gram ',3000),('Groundnut',3700),('Jowar',1500),('Jute',2200),('Maize ',1175),('Masur',2900),('Moong ',4400),('Mustard',3000),('Niger seed',3500),('Paddy',1250),('Ragi',1500),('Rapeseed',3000),('Safflower ',2800),('Sesamum',4200),('Soyabean',2200),('Sugarcane',170),('Sunflower ',3700),('Urad',4300),('Wheat ',1350);
/*!40000 ALTER TABLE `minimum_support_price` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-11 10:29:20
