__author__ = 'ankush'

import cherrypy
import MySQLdb
import os
import farmer_confidence_generator
import crop_confidence_generator
import json
from jinja2 import Environment, FileSystemLoader

dir = os.path.dirname(__file__) 
env = Environment(loader=FileSystemLoader([dir]))



class App(object):
    exposed = True


    @cherrypy.tools.allow(methods=['POST','GET'])
    @cherrypy.expose
    def result(self, **params):
        print params
        age = params.get('age')
        education_qualification = params.get('education_qualification')
        sex = params.get('sex')
        farming_exp = params.get('farming_exp')
        previous_loan_availed = params.get('previous_loan_availed')
        loan_status = params.get('loan_status')
        income = params.get('income')
        land_owned = params.get('land_owned')
        criminal_history = params.get('criminal_history')
        address = params.get('address')
        district = params.get('district')
        state = params.get('state')
        crop = params.get('crop')
        month = params.get('month')
        
        farmer_confidence = farmer_confidence_generator.get_confidence(age, education_qualification, sex, farming_exp, previous_loan_availed, loan_status, income, land_owned, criminal_history)

        result = {}
        result['farmer_confidence'] = farmer_confidence

        crop_confidence = crop_confidence_generator.get_confidence(address, district, state, crop, month)
        result['crop_confidence'] = crop_confidence

        result['overall_confidence'] = (2*result['crop_confidence']['overall_score'] + farmer_confidence)/3
        # return json.dumps(result, indent=True)
        return env.get_template('result.html').render(input=result)


if __name__ == '__main__':
    #cherrypy.config.update({'server.socket_host': 'localhost', 'server.socket_port': 8084,'server.thread_pool' = 30 })
    cherrypy.server.socket_host = "0.0.0.0"
    cherrypy.server.socket_port = 8857

    cherrypy.quickstart(App(), '/', "app.conf")

