import requests
import json
import numpy as np 
from collections import Counter
from math import radians, cos, sin, asin, sqrt

googleKey = "AIzaSyDunT8ZNchDXEM7yLwOt2vBWILUQHw9DMM"
weatherKey = "f44e8f22ec9d3df1a0b401ab46917589"
geoLocationApi = "https://maps.googleapis.com/maps/api/geocode/json"
weatherDataApi = "http://api.openweathermap.org/data/2.5/forecast/daily"


def haversine(lon1, lat1, lon2, lat2):
	"""
	Calculate the great circle distance between two points 
	on the earth (specified in decimal degrees)
	"""
	# convert decimal degrees to radians 
	lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
	# haversine formula 
	dlon = lon2 - lon1 
	dlat = lat2 - lat1 
	a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
	c = 2 * asin(sqrt(a)) 
	km = 6367 * c
	return km



def geoCoordExt(address):
	param = {}
	param["address"] =address
	param["key"] = googleKey
	r = requests.get(geoLocationApi,params=param)
	data = json.loads(r.content)
	GeoCoordPlace = data["results"][0]["geometry"]["location"]
	return GeoCoordPlace 


def weatherExt(geoCoord,noDays):
	param= {}
	param["lat"] = str(int(geoCoord["lat"]))
	param["lon"] = str(int(geoCoord["lng"]))
	param["cnt"] = str(noDays)
	param["appid"] = weatherKey
	param["mode"] = "json"
	#print param
	r = requests.get(weatherDataApi,params=param)
	# print r.url
	data = json.loads(r.content)
	weatherData = data["list"]
	
	minTemp = []
	maxTemp = []
	pressure = []
	humidity = []
	weather = []
	for data in weatherData:
		minTemp.append(data["temp"]["min"])
		maxTemp.append(data["temp"]["max"])
		pressure.append(data["pressure"])
		humidity.append(data["humidity"])
		weather.append(data["weather"][0]["main"])
	b = Counter(weather)

	output= {}
	output["min_temp"] =int(np.mean(minTemp)) - 273
	output["max_temp"] = int(np.mean(maxTemp)) - 273
	output["avg_temp"] = (output["min_temp"]+output["max_temp"])/2.0
	output["humidity"] = int(np.mean(humidity))
	output["pressure"] = int(np.mean(pressure))
	w,c = b.most_common(1)[0]
	output["weather"] =  w

	return output

def  nearestFireFighter(city,state,geoCoord):
	stringFire = "fire station " + city +" "+state +" India"
	param = {}
	param["address"] =stringFire
	param["key"] = googleKey
	r = requests.get(geoLocationApi,params=param)
	data = json.loads(r.content)
	distanceList = []
	for fireStation in data["results"]:
		newStation = fireStation["geometry"]["location"]
		distance = haversine(geoCoord["lng"], geoCoord["lat"], newStation["lng"], newStation["lat"])
		distanceList.append(distance)

	fireStation = min(distanceList)
	return fireStation


def getInfo(address,city,state):
	coord = geoCoordExt(address)
	weatherData = weatherExt(coord,16)
	nearestFireStation = nearestFireFighter(city,state,coord)
	if nearestFireStation == 0.0:
		fireStation = []
	else:
		fireStation = nearestFireStation
	dictOut = {}
	dictOut["weather"] =weatherData
	dictOut["nearest_fire_station"] = fireStation
	return dictOut

if __name__ == "__main__":
	address = raw_input("Please Enter the location address")
	city = raw_input("Please Enter the city")
	state = raw_input("Please Enter the state")
	coord = geoCoordExt(address)
	print coord
	print weatherExt(coord,16)
	print nearestFireFighter(city,state,coord)