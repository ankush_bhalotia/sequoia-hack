__author__ = 'ankush'

import requests
import json
from lxml import html
import sys
import csv
import MySQLdb
import time


if __name__=='__main__':

	db = MySQLdb.connect(host="localhost", user="root", passwd="", db="sequoia_hack")
	cursor = db.cursor()

	data_types = {'1': 'Precipitation', '2': 'Minimum temperature', '3': 'Average temperature', '4': 'Maximum temperature', 
				'5': 'Cloud cover', '6': 'Vapour pressure', '7': 'Wet day frequency', '8': 'Diurnal temperature range', 
				'9': 'Ground frost frequency', '10': 'Reference Crop Evapotranspiration', '11': 'Potential Evapotranspiration'}

	 
	states = {'24': 'GUJARAT', '25': 'DAMAN & DIU', '26': 'DADRA & NAGAR HAVELI', '27': 'MAHARASHTRA', '20': 'JHARKHAND', '21': 'ORISSA', '22': 'CHHATTISGARH', 
			'23': 'MADHYA PRADESH', '28': 'ANDHRA PRADESH', '29': 'KARNATAKA', '1': 'JAMMU & KASHMIR', '3': 'PUNJAB', '2': 'HIMACHAL PRADESH', '5': 'UTTARANCHAL', '4': 'CHANDIGARH', 
			'7': 'DELHI', '6': 'HARYANA', '9': 'UTTAR PRADESH', '8': 'RAJASTHAN', '11': 'SIKKIM', '10': 'BIHAR', '13': 'NAGALAND', '12': 'ARUNACHAL PRADESH', '15': 'MIZORAM', 
			'14': 'MANIPUR', '17': 'MEGHALAYA', '16': 'TRIPURA', '33': 'TAMIL NADU', '18': 'ASSAM', '31': 'LAKSHADWEEP', '30': 'GOA', '35': 'ANDAMAN & NICOBAR ISLANDS', 
			'34': 'PONDICHERRY', '19': 'WEST BENGAL', '32': 'KERALA'}


	for state_id, state_name in states.iteritems():

		page = requests.get('http://www.indiawaterportal.org/met_data/data/state/' + state_id)
		tree = html.fromstring(page.content)

		district_names = tree.xpath('//option/text()')
		district_ids = tree.xpath('//option/@value')
		del district_ids[0]
		del district_names[0]

		districts = {}
		for i in range(len(district_ids)):
			districts[district_ids[i]] = district_names[i]


		for district_id, district_name in districts.iteritems():

			for data_type_id, data_type_name in data_types.iteritems():
				print state_name, district_name, data_type_name
				page = requests.get('http://www.indiawaterportal.org/met_data/data/csv/%s/%s/%s/1970/2002' %(state_id, district_id, data_type_id))
				time.sleep(1)
				fw = open('temp/temp.tsv', 'w')
				fw.write(page.content)
				fw.close()
				fo = open('temp/temp.tsv')
				reader = csv.reader(fo, delimiter='\t')
				for row in reader:
					if not row:
						continue
					if not row[0]:
						continue
					if row[0].lower().strip()=='year':
						continue

					months = {1:'Jan', 2:'Feb', 3:'Mar', 4:'Apr', 5:'May', 6:'Jun', 7:'Jul', 8:'Aug', 9:'Sep', 10:'Oct', 11:'Nov', 12:'Dec'}
					for month_id, month_name in months.iteritems():
						write_query = "insert into met_data(data_type, state, district, year, month, value) values('%s', '%s', '%s', %s, '%s', %s)" %(data_type_name, state_name, district_name, str(row[0]), month_name, float(row[month_id]))
						try:
							cursor.execute(write_query)
						except:
							print write_query
						

			db.commit()
			



