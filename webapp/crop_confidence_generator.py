__author__ = 'shubham'

import json
import MySQLdb
from weather_prediction_api import getInfo
import sys

class DB:
	conn = None

	def connect(self):
		#self.conn = MySQLdb.connect("localhost", "semantics_admin", "Ti0BXIDo", "sequoia_hack")
		self.conn = MySQLdb.connect("localhost", "dw_semantics", "87fu49gu5g", "sequoia_hack")

	def commit(self):
		try:
			self.conn.commit()
		except (AttributeError, MySQLdb.OperationalError):
			self.connect()
			self.conn.commit()

	def query(self, sql):
		try:
			cursor = self.conn.cursor()
			cursor.execute(sql)
		except (AttributeError, MySQLdb.OperationalError):
			self.connect()
			cursor = self.conn.cursor()
			cursor.execute(sql)
		return cursor


db = DB()

def avgTemp(minTemp,maxTemp):
	return (minTemp+maxTemp)//2

def compareFun(value,comparedList):
		if ((value>=comparedList[0]) and (value <= comparedList[1])):
			return 3
		elif ((value>=(comparedList[0]-3)) and (value <= (comparedList[1]+3))):
			return 2
		else:
			return 1


def isExist(crop,listCrop):
	if crop in listCrop:
		return 3
	else:
		return 1



def dataInput():
	outputDict = {}
	address = raw_input("please Enter full address")
	outputDict["add"] = address
	state = raw_input("please enter the state")
	outputDict["state"] = state
	district = raw_input("please enter the district")
	outputDict["district"] = district
	crop = raw_input("please enter the crop to be planted")
	outputDict["crop"] = crop
	soawing = raw_input("please enter the sowing month")
	outputDict["sowing"] = sowing
	harvest = raw_input("please enter the harvest month")
	outputDict["harvest"]= harvest

	return outputDict




def soilDataJson(fileName="./soil_data.json",state=None):
	soilData = json.load(open(fileName))
	try:
		return soilData[state.upper()]["crops_supported"]
	except Exception as e:
		return None




def futureWeatherInfo(address,district,state):
	pass




def cropLocationTempCompare(district,state,month):
	try:
		searchString = "select month, avg(value) from met_data where data_type='Average temperature' and district='%s' and year in ('1996','1997','1998','1999', '2000') and month='%s' and state='%s'  group by month"%(district,month[:3],state)
		cursor = db.query(searchString)
		avgTemp = cursor.fetchone()[1]
		return int(avgTemp)
	except Exception as e:
		return None
	



def cropLocationRainfallCompare(district, state,month):
	try:
		monthList = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]
		prevMonth = monthList[monthList.index(month.upper()[:3])-1]
		nextMonth = monthList[monthList.index(month.upper()[:3])+1]

		#print prevMonth, nextMonth
		searchString1 = "select month, avg(value) from met_data where data_type='Precipitation' and district='%s' and year in ('1996','1997','1998','1999', '2000') and month='%s' and state='%s'  group by month"%(district,month[:3],state)
		cursor = db.query(searchString1)
		avgTemp1 = cursor.fetchone()[1]

		#print avgTemp1

		searchString2 = "select month, avg(value) from met_data where data_type='Precipitation' and district='%s' and year in ('1996','1997','1998','1999', '2000') and month='%s' and state='%s'  group by month"%(district,prevMonth,state)
		cursor = db.query(searchString2)
		avgTemp2 = cursor.fetchone()[1]

		#print avgTemp2

		searchString3 = "select month, avg(value) from met_data where data_type='Precipitation' and district='%s' and year in ('1996','1997','1998','1999', '2000') and month='%s' and state='%s'  group by month"%(district,nextMonth,state)
		cursor = db.query(searchString3)
		avgTemp3 = cursor.fetchone()[1]
		#print avgTemp3
		#print (int(avgTemp1)+int(avgTemp2)+int(avgTemp3))/3.0
		return (int(avgTemp1)+int(avgTemp2)+int(avgTemp3))/3.0
	except Exception as e:
		return None


	

def cropDetailExt(seed):
	try:
		searchString = "SELECT * FROM crop_climate_requirement WHERE crop=\'%s\'"%(seed)
		cursor = db.query(searchString)
		results = cursor.fetchone()
		maxTemp =  int(results[1])
		minTemp =  int(results[2])
		rainFall =int(results[3])
		return {"mintemp":minTemp,"maxtemp":maxTemp,"rainfall":rainFall}
	except Exception as e:
		print repr(e)
		return None




def surplusDefCrop(seed):
	try:
		searchString = "SELECT * FROM crop_requirement_and_availability WHERE crop=\'%s\'"%(seed)
		cursor = db.query(searchString)
		results = cursor.fetchone()
		return results*-1
	except Exception as e:
		return None




def get_confidence(address= None, district =None, state = None, crop=None, month = None, apiFlag=False):
	score = 0
	count =0
	finalScore = {}
	finalScore["deficit"] = surplusDefCrop(crop)
	outputDict = cropDetailExt(crop)
	if outputDict is not None:
		rangeIdleTemp = [outputDict["mintemp"],outputDict["maxtemp"]]
		rangeIdleRain = [outputDict["rainfall"]-10,outputDict["rainfall"]+10]


		rainData = cropLocationRainfallCompare(district,state,month)
		if rainData is not None:
			rainScore=compareFun(rainData,rangeIdleRain)
			score+=rainScore
			finalScore["rain_score"] = rainScore/3.0
			count+=1
		else:
			finalScore["message_rainfall"] = "district rainfall is not in database"
			pass

		tempData = cropLocationTempCompare(district,state,month)
		if tempData is not None:
			tempScore=compareFun(tempData,rangeIdleTemp)
			score+=tempScore
			finalScore["temp_score"] = tempScore/3.0
			count+=1
		else:
			finalScore["message_temperature"] = "district temperature is not in database"
			pass


		cropSupportedSoil = soilDataJson(state=state)
		if cropSupportedSoil is not None:
			soilScore=isExist(crop.lower(),cropSupportedSoil)
			score+=soilScore
			finalScore["soil_score"] = soilScore/3.0
			count+=1
		else:
			finalScore["message_soil"] = "soil data problem"
		if apiFlag:
			outDict = getInfo(address=address,city=district,state=state)
			if len(outDict["nearest_fire_station"])>0:
				if outDict["nearest_fire_station"] < 5:
					score+=3
					finalScore["nearest_fire_station"] = 1
					count+=1
					
				elif outDict["nearest_fire_station"] < 10:
					score+=2
					finalScore["nearest_fire_station"] = 2/3.0
					count+=1
				else:
					score+=1
					finalScore["nearest_fire_station"] = 1/3.0
					count+=1

				finalScore["overall_score"]=score/float(count*3)
				finalScore["status"] = "success"
				return finalScore
			else:
				finalScore["overall_score"]=score/float(count*3)
				finalScore["status"] = "success"
				return finalScore
		else:
			finalScore["overall_score"]=score/float(count*3)
			finalScore["status"] = "success"
			return finalScore

	else:
		return {"status": "failure", "message": "crop not in database"}



if __name__ =="__main__":
	print get_confidence(address= None, district ="KOLKATA", state = "WEST BENGAL", crop=sys.argv[1], month = "JULY", apiFlag=False)

	# data = surplusDefCrop("GRAM")
	# print int(data[3])/float(data[1])




